from django.shortcuts import render
from django.http import HttpResponse
from fingerprint.models import Jari
# Create your views here.

def welcome(request, extra_context={}):
	extra_context.update({"data": Jari.objects.all()})
	return render(request, "home.html", extra_context)
	# return HttpResponse("Selamat Datang...")