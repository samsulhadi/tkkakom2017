from django.contrib import admin

# Register your models here.
from fingerprint.models import Jari, Absensi

class JariAdmin(admin.ModelAdmin):
	list_display = ('nama_jari', 'jari', 'keterangan')
	search_fields = ('nama_jari', 'keterangan')

admin.site.register(Jari, JariAdmin)

class AbsensiAdmin(admin.ModelAdmin):
	list_display = ('nama_user', 'jari','waktu')
	readonly_fields = ('jari', 'waktu')

	def nama_user(self, obj):
		return obj.jari.user

	def has_add_permission(self, request):
		return False

admin.site.register(Absensi, AbsensiAdmin)