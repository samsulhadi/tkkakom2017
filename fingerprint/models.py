from django.db import models
from django.contrib.auth.models import User

# Create your models here.
class Jari(models.Model):
	user = models.ForeignKey(User)
	nama_jari = models.CharField(max_length=20, verbose_name="Nama Jari")
	jari = models.TextField(verbose_name="Fingerprint Jari")
	keterangan = models.CharField(max_length=100, null=True, blank=True)

	def __unicode__(self):
		return str(self.user.first_name)+' '+str(self.nama_jari)

	class Meta:
		ordering = ['-jari',]
		verbose_name = "Jari"
		verbose_name_plural = "Jari"

class Absensi(models.Model):
	jari = models.ForeignKey(Jari, verbose_name="Jari")
	waktu = models.DateTimeField("Waktu")

	class Meta:
		verbose_name = "Data Absensi"
		verbose_name_plural = "Data Absensi"